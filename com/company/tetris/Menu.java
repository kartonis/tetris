package com.company.tetris;

import com.company.tetris.model.Field;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Menu extends JFrame
{
    JButton newGame, exit, highScores, about;

    public Menu()
    {
        newGame = new JButton("New game");
        //newGame.addActionListener((ActionEvent e) -> new Thread(this::newGame).start());
        exit = new JButton("Exit");
        //exit.addActionListener((ActionEvent e) -> System.exit(0));
        highScores = new JButton("High score");
//        highScores.addActionListener((ActionEvent e) ->  {
//
//            JFrame record = new JFrame("High score");
//            Field gameField = new Field();
//            record.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//            record.setBackground(Color.WHITE);
//            record.setSize(300, 400);
//            record.setLocationRelativeTo(null);
//            record.setLayout(new GridLayout(10, 1));
//            List<Integer> listWithRecords = gameField.getRecord();
//            for(int i = 0; i < listWithRecords.size(); ++i){
//                record.add(new JLabel(listWithRecords.get(i).toString(), SwingConstants.CENTER));
//            }
//            record.setVisible(true);
//
//        });
        about = new JButton("About");
//        about.addActionListener((ActionEvent e) -> {
//            JFrame frameAbout = new JFrame("About");
//            JTextArea text = new JTextArea("Tetriminos are game pieces shaped like tetrominoes, geometric shapes composed of four square blocks each. A random sequence of Tetriminos fall down the playing field (a rectangular vertical shaft, called the \\\"well\\\" or \\\"matrix\\\"). The objective of the game is to manipulate these Tetriminos, by moving each one sideways and/or rotating by quarter-turns, so that they form a solid horizontal line without gaps. When such a line is formed, it disappears and any blocks above it fall down to fill the space");
//            text.setLineWrap(true);
//            text.setEnabled(false);
//            frameAbout.add(text);
//            frameAbout.setBackground(Color.WHITE);
//            frameAbout.setSize(300, 400);
//            frameAbout.setLocationRelativeTo(null);
//            frameAbout.setVisible(true);
//        });

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new GridLayout(4,1));
        getContentPane().add(newGame);
        getContentPane().add(highScores);
        getContentPane().add(about);
        getContentPane().add(exit);
        setBackground(Color.WHITE);
        setSize(300, 400);
        setLocationRelativeTo(null);
        setVisible(true);
        setTitle("Menu");
    }
    private void newGame(){
        Lock l = new ReentrantLock();
        l.lock();
        Field field = new Field();
        Condition keyPressed = l.newCondition();

        User u = new User(field, l, keyPressed);
        GraphicsModule g = new GraphicsModule(field, u);

        long freq = 1000;
        //SwingUtilities.invokeLater();
        while(!field.getLost()) {
            if (!field.isPaused()) {
                long start = System.currentTimeMillis(),
                        stop = start;

                while (stop - start < freq) {
                    try {
                        keyPressed.await(freq, TimeUnit.MILLISECONDS);
                        stop = System.currentTimeMillis();
                        g.repaintField();
                    } catch (Exception e) {
                        break;
                    }
                }
                g.update();
            }
            else {
                try {
                    keyPressed.await();
                } catch (InterruptedException e) {
                    break;
                }
            }
        }
        g.showScore();
        try {
            Thread.sleep(5000);
        }
        catch (Exception e){
        }
        g.destroy();
    }
    public void initPress(){
        newGame.addActionListener((ActionEvent e) -> new Thread(this::newGame).start());
        exit.addActionListener((ActionEvent e) -> System.exit(0));
        highScores.addActionListener((ActionEvent e) ->  {

            JFrame record = new JFrame("High score");
            Field gameField = new Field();
            record.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            record.setBackground(Color.WHITE);
            record.setSize(300, 400);
            record.setLocationRelativeTo(null);
            record.setLayout(new GridLayout(10, 1));
            List<Integer> listWithRecords = gameField.getRecord();
            for(int i = 0; i < listWithRecords.size(); ++i){
                record.add(new JLabel(listWithRecords.get(i).toString(), SwingConstants.CENTER));
            }
            record.setVisible(true);

        });

        about.addActionListener((ActionEvent e) -> {
            JFrame frameAbout = new JFrame("About");
            JTextArea text = new JTextArea("Tetriminos are game pieces shaped like tetrominoes, geometric shapes composed of four square blocks each. A random sequence of Tetriminos fall down the playing field (a rectangular vertical shaft, called the \\\"well\\\" or \\\"matrix\\\"). The objective of the game is to manipulate these Tetriminos, by moving each one sideways and/or rotating by quarter-turns, so that they form a solid horizontal line without gaps. When such a line is formed, it disappears and any blocks above it fall down to fill the space");
            text.setLineWrap(true);
            text.setEnabled(false);
            frameAbout.add(text);
            frameAbout.setBackground(Color.WHITE);
            frameAbout.setSize(300, 400);
            frameAbout.setLocationRelativeTo(null);
            frameAbout.setVisible(true);
        });
    }


}