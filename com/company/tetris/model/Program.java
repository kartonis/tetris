package com.company.tetris.model;

import com.company.tetris.model.Field;

public class Program {
    Field field = new Field();

    public final int[][] getFieldFromProgram(){
        return field.getField();
    }
    public void nextStepFromProgram(){
        field.nextStep();
    }

    public void logic(int f) {
        switch (f) {
            case 0:
                field.rotateFigure();
                break;
            case 1:
                field.shiftFigureLeft();
                break;
            case 2:
                field.shiftFigureRight();
                break;
            case 3:
                field.shiftFigureDown();
                break;
        }
    };

    public boolean getLastFlag(){
            return  field.getLost();
    };
}
