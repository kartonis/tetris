package com.company.tetris.model;

import com.company.tetris.model.figures.*;

import java.io.*;
import java.util.*;

public class Field
{
    File recordFile;
    private boolean pause = false;

    private Figure figure;
    private int[][] field;

    private List<Integer> records;
    private boolean lost = false;
    private boolean landed;
    private final int defaultColor = -1;
    private int color = -1;
    private int[] x, y;
    private final int MaxY = 20;
    private final int MaxX = 10;
    private int score = 0;
    private int[] countFilledCellInLine = new int[MaxY];

    private void fillAllNegativeNumber()
    {
        for (int i = 0; i < MaxY; ++i) {
            for (int j = 0; j < MaxX; ++j) {
                field[i][j] = defaultColor;
            }
        }
    }
    private void fillFigureCoords(int val){
        for (int i = 0; i < 4; ++i){
            field[y[i]][x[i]] = val;
        }
    }
    public Field()
    {
        landed = true;
        field = new int[MaxY][MaxX];
        fillAllNegativeNumber();
        for (int i = 0; i < MaxY; ++i){
            countFilledCellInLine[i] = 0;
        }
        records = new LinkedList<>();

        recordFile = new File("Record");
        if (!recordFile.exists()){
            try {
                recordFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
            for (int i = 0; i < 10; ++i){
                records.add(0);
            }
        }
        else
        {
            Scanner scanner;
            try {
                scanner = new Scanner(recordFile);
                while (scanner.hasNext()){
                    records.add(scanner.nextInt());
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        while (records.size() < 10)
            records.add(0);
    }

    public void createFigure()
    {
        int randomX = new Random().nextInt(6);
        //int randomX = 0;
        switch (randomX) {
            case 0:
                figure = new I();
                break;
            case 1:
                figure = new T();
                break;
            case 2:
                figure = new O();
                break;
            case 3:
                figure = new S();
                break;
            case 4:
                figure = new J();
                break;
            case 5:
                figure = new L();
                break;
            case 6:
                figure = new Z();
                break;
        }
        color = randomX;
        x = figure.getX();
        y = figure.getY();
        fillFigureCoords(color);
    }
   private void shiftLineDown(int y)
   {
       if (countFilledCellInLine[y] == 0) {
           for (int k = y; k > 0; k--) {
               countFilledCellInLine[k] = countFilledCellInLine[k - 1];

               for (int i = 0; i < MaxX; ++i)
                   field[k][i] = field[k - 1][i];
           }
           countFilledCellInLine[0] = 0;

           for (int i = 0; i < MaxX; ++i)
               field[0][i] = defaultColor;
       }
   }
    private void DestroyLine(int y){

        for (int j = 0; j < MaxX; ++j) {
            field[y][j] = defaultColor;
        }
        countFilledCellInLine[y] = 0;
    }

    private boolean partOfFigure(int y_coord, int x_coord){
        int size = figure.getSize();
        for (int i = 0; i < size; ++i) {
            if (y_coord == y[i] && x_coord == x[i])
                return true;
        }
        return false;
    }

    private void addScore(int destroyedLines){
        //System.out.println(score);
        switch (destroyedLines){
            case 1:
                score += 100;
                break;
            case 2:
                score +=200;
                break;
            case 3:
                score += 700;
                break;
            case 4:
                score += 1500;
                break;
            default:
        }
    }

    private void putRecord(int score){
        records.add(score);
        records.sort(Comparator.reverseOrder());
        try {
            ListIterator<Integer> it = records.listIterator(0);

            FileWriter writer = new FileWriter(recordFile);
            for (int i = 0; i < 10; ++i){
                writer.write(it.next().toString() + '\n');
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public List<Integer> getRecord(){
        return records;
    }
    public void shiftFigureDown()
    {
        int size = figure.getSize();
        for (int i = 0; i < size; ++i) {
            if (y[i] + 1 == MaxY
                    || (field[y[i] + 1][x[i]] != defaultColor
                    && !partOfFigure(y[i] + 1, x[i]))) {
                landed = true;
                int destroyedLines = 0;
                for (int j = 0; j < size; ++j) {
                    countFilledCellInLine[y[j]]++;

                    if (countFilledCellInLine[y[j]] == MaxX) {

                        int recount = 0;
                        for (int k = 0; k < MaxX; k++)
                        {
                            recount += (field[y[j]][k] == -1 ? 0 : 1);
                        }

                        if (recount == MaxX)
                        {
                            DestroyLine(y[j]);
                            destroyedLines++;
                            shiftLineDown(y[j]);
                        }

                        else countFilledCellInLine[y[j]] = recount;
                    }
                    addScore(destroyedLines);

                    if (y[j] == 0) {
                        lost = true;
                        putRecord(score);
                        return;
                    }
                }

                return;
            }
        }

        fillFigureCoords(defaultColor);
        figure.shiftDown();
        fillFigureCoords(color);
    }

    public void shiftFigureLeft()
    {
        int size = figure.getSize();
        for (int i = 0; i < size; ++i) {
            if (x[i] == 0 || (field[y[i]][x[i]-1] != defaultColor && !partOfFigure(y[i], x[i]-1))) {
                return;
            }
        }
        fillFigureCoords(defaultColor);
        figure.shiftLeft();
        fillFigureCoords(color);
    }

    public void shiftFigureRight(){
        int size = figure.getSize();
        for (int i = 0; i < size; ++i) {
            if (x[i] + 1 == MaxX || (field[y[i]][x[i] + 1] != defaultColor && !partOfFigure(y[i], x[i] + 1))) {
                return;
            }
        }
        fillFigureCoords(defaultColor);
        figure.shiftRight();
        fillFigureCoords(color);
    }

    public void rotateFigure(){
        fillFigureCoords(defaultColor);
        figure.rotate();

        int size = figure.getSize();
        for (int i = 0; i < size; i++) {
            if (y[i] == MaxY || y[i] == -1 || x[i] == -1 || x[i] == MaxX || field[y[i]][x[i]] != defaultColor){
                figure.undo();
                break;
            }
        }
        fillFigureCoords(color);
    }
    public void nextStep(){
        if (landed) {
            this.createFigure();
            landed = false;
        } else shiftFigureDown();
    }
    public final int[][] getField()
    {
        return field;
    }
    public int getScore(){
        return score;
    }

    public boolean getLost(){
        return lost;}
    public void setPause(){
        pause = !pause;
    }

    public boolean isPaused()
    {
        return pause;
    }
}
