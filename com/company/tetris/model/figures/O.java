package com.company.tetris.model.figures;

public class O extends Figure {
    public O() {
        super(4);

        numState = 1;
        currState = 0;

        x[0] = 4;
        y[0] = 0;
        x[1] = 5;
        y[1] = 0;
        x[2] = 4;
        y[2] = 1;
        x[3] = 5;
        y[3] = 1;
    }
    @Override
    protected void stateRotate(){}
}
