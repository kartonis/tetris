package com.company.tetris.model.figures;

public class I extends Figure {
    public I()
    {
        super(4);

        currState = 0;
        numState = 2;

        x[0] = 3;
        y[0] = 0;
        x[1] = 4;
        y[1] = 0;
        x[2] = 5;
        y[2] = 0;
        x[3] = 6;
        y[3] = 0;
    }
    @Override
    protected void stateRotate(){
        int med = 0;
        int prev = 0;
        if (currState == 0){
            for(int i = 0; i < size; ++i){
                med+=y[i];
            }
            prev = x[0] - 1;
            med /= size;
            for (int i = 0; i < size; ++i){
                x[i] = prev + i;
                y[i] = med;
            }
        }
        else{
                for(int i = 0; i < size; ++i){
                    med+=x[i];
                }
                med /= size;
                prev = y[0] - 1;
                for (int i = 0; i < size; ++i){
                    x[i] = med;
                    y[i] = prev + i;
                }
            }
        }
    }

