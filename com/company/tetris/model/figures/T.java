package com.company.tetris.model.figures;

public class T extends Figure {
    public T() {
        super(4);

        numState = 4;
        currState = 0;

        x[0] = 4;
        y[0] = 0;
        x[1] = 5;
        y[1] = 0;
        x[2] = 6;
        y[2] = 0;
        x[3] = 5;
        y[3] = 1;
    }
    @Override
    protected void stateRotate(){
        int med = 0;
        int prev = 0;
        switch (currState){
            case 0:
                for(int i = 0; i < size; ++i){
                    med+=y[i];
                }
                prev = x[0] - 1;
                med /= size;
                for (int i = 0; i < size-1; ++i){
                    x[i] = prev + i;
                    y[i] = med;
                }
                x[3] = x[1];
                y[3] = med + 1;
                break;
            case 3:
                for(int i = 0; i < size; ++i){
                    med+=x[i];
                }
                med /= size;
                prev = y[0] - 1;
                for (int i = 0; i < size - 1; ++i){
                    x[i] = med;
                    y[i] = prev + i;
                }
                x[3] = med+1;
                y[3] = y[1];
                break;
            case 2:
                for(int i = 0; i < size; ++i){
                    med+=y[i];
                }
                prev = x[0] - 1;
                med /= size;
                for (int i = 0; i < size-1; ++i){
                    x[i] = prev + i;
                    y[i] = med;
                }
                x[3] = x[1];
                y[3] = med - 1;
                break;
            case 1:
                for(int i = 0; i < size; ++i){
                    med+=x[i];
                }
                med /= size;
                prev = y[0] - 1;
                for (int i = 0; i < size - 1; ++i){
                    x[i] = med;
                    y[i] = prev + i;
                }
                x[3] = med-1;
                y[3] = y[1];
                break;
        }
    }
}
