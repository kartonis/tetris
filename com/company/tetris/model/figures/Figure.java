package com.company.tetris.model.figures;

public abstract class Figure {

    private int[] xSave;
    private int[] ySave;
    protected int[] x;
    protected int[] y;

    protected int size;

    protected int currState;
    protected int numState;

    public Figure(int size)
    {
        this.size = size;
        xSave = new int[size];
        ySave = new int[size];
        x = new int[size];
        y = new int[size];
    }

    public final int[] getX(){ return x; }

    public final int[] getY(){ return y; }

    public final int getSize(){ return size; }

    public void shiftDown(){
        for (int i = 0; i < size; ++i){
            y[i]++;
        }
    }

    public void shiftRight(){
        for (int i = 0; i < size; ++i){
            x[i]++;
        }
    }

    public void shiftLeft(){
        for (int i = 0; i < size; ++i){
            x[i]--;
        }
    }

    protected abstract void stateRotate();

    public void rotate(){
        currState = (currState+1)%numState;
        save();
        stateRotate();
    }

    private void save(){

        for (int i = 0; i < size; ++i){
            xSave[i] = x[i];
            ySave[i] = y[i];
        }
    }

    public void undo()
    {
        for (int i = 0; i < size; i++)
        {
            x[i] = xSave[i];
            y[i] = ySave[i];
        }
        currState = (currState - 1 + numState) % numState;
    }
}