package com.company.tetris.model.figures;

public class Z extends Figure {
    public Z() {
        super(4);

        numState = 2;
        currState = 0;

        x[0] = 4;
        y[0] = 0;
        x[1] = 5;
        y[1] = 0;
        x[2] = 5;
        y[2] = 1;
        x[3] = 6;
        y[3] = 1;
    }
    @Override
    protected void stateRotate(){
        int med = 0;
        int prev1 = 0, prev2 = 0, prev3 = 0;
        switch (currState) {

            case 0:
                med = y[1];
                int prev_medY = med - 1;

                prev1 = x[2]-1;
                prev2 = x[2];
                prev3 = x[0];

                y[0] = prev_medY;
                y[1] = prev_medY;
                y[2] = med;
                y[3] = med;

                x[0] = prev1;
                x[1] = prev2;
                x[3] = prev3;

                shiftRight();
                break;

            case 1:
                med = x[1];
                int prev_med = med-1;

                prev2 = y[2];
                prev3 = y[2]+ 1;

                x[0] = med;
                x[2] = prev_med;
                x[3] = prev_med;

                y[1] = prev2;
                y[3] = prev3;
                break;
        }
    }
}