package com.company.tetris;

import com.company.tetris.model.Field;


import javax.swing.*;
import java.awt.*;
import java.awt.Color;


public class GraphicsModule extends JFrame {
    private final int picsel = 32;
    private final int border = 22;
    private Field field;
    private final int MaxX = 10;
    private final int MaxY = 20;
    private int[][] colorField;

    public GraphicsModule(Field f, User u) {
        field = f;
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        colorField = field.getField();
        addKeyListener(u);
        setBackground(Color.WHITE);
        setSize(MaxX * picsel, MaxY * picsel + border);
        setLocationRelativeTo(null);
        setVisible(true);
        setTitle("Tetris");

    }

    public void repaintField(){
        repaint();
    }
    public void update() {
        field.nextStep();
        repaint();
    }

    public void destroy() {
        setVisible(false);
        dispose();
        System.exit(0);
    }

    ;
    @Override
    public void paint(Graphics g) {
        Image buffer = createImage(getWidth(), getHeight());
        Graphics gd = buffer.getGraphics();
        update(gd);
        g.drawImage(buffer, 0, 0, null);
    }
    private void border(int[] x, int [] y, int i, int j){
        x[0] = j * picsel;
        y[0] = i * picsel + border;
        x[1] = (j + 1) * picsel;
        y[1] = i * picsel + border;
        x[2] = (j + 1) * picsel;
        y[2] = i * picsel + picsel + border;
        x[3] = j * picsel;
        y[3] = i * picsel + picsel + border;
    }
    private void square (int[] x, int[] y, int i, int j)
    {
        x[0] = j * picsel + 1;
        y[0] = i * picsel + border + 1;
        x[1] = (j + 1) * picsel - 2;
        y[1] = i * picsel + border + 1;
        x[2] = (j + 1) * picsel - 2;
        y[2] = i * picsel + picsel + border - 2;
        x[3] = j * picsel + 1;
        y[3] = i * picsel + picsel + border - 2;
    }

    @Override
    public void update(Graphics g) {
        int k = 0;
        Color white = new Color(255, 255, 255);
        Color red = new Color(255, 0, 0);
        Color blue = new Color(0, 0, 255);
        Color green = new Color(0, 255, 0);
        Color yellow = new Color(255, 255, 0);
        Color purple = new Color(255, 0, 255);
        Color almostBlue = new Color(0, 255, 255);
        Color orange = new Color(255, 143, 32);
        Color black = new Color(0, 0, 0);
        Color gray = new Color(180, 180, 180, 112);


        int[] x = {-1, -1, -1, -1};
        int[] y = {-1, -1, -1, -1};

        for (int i = 0; i < MaxY; ++i){
            for (int j = 0; j < MaxX; ++j){

                Polygon poly;

                border(x, y, i, j);
                g.setColor((colorField[i][j] == -1 ? gray : black));
                poly = new Polygon(x, y, 4);
                g.drawPolygon(poly);
                g.fillPolygon(poly);

                square(x, y, i, j);

                switch (colorField[i][j]){
                    case -1:
                    g.setColor(white);
                    poly = new Polygon(x, y, 4);
                    g.drawPolygon(poly);
                    g.fillPolygon(poly);
                    break;

                    case 0:

                    g.setColor(red);
                    poly = new Polygon(x, y, 4);
                    g.drawPolygon(poly);
                    g.fillPolygon(poly);
                    break;

                    case 1:
                        g.setColor(almostBlue);
                        poly = new Polygon(x, y, 4);
                        g.drawPolygon(poly);
                        g.fillPolygon(poly);
                        break;

                    case 2:
                        g.setColor(blue);
                        poly = new Polygon(x, y, 4);
                        g.drawPolygon(poly);
                        g.fillPolygon(poly);
                        break;

                    case 3:
                        g.setColor(purple);
                        poly = new Polygon(x, y, 4);
                        g.drawPolygon(poly);
                        g.fillPolygon(poly);
                        break;

                    case 4:
                        g.setColor(green);
                        poly = new Polygon(x, y, 4);
                        g.drawPolygon(poly);
                        g.fillPolygon(poly);
                        break;

                    case 5:
                        g.setColor(yellow);
                        poly = new Polygon(x, y, 4);
                        g.drawPolygon(poly);
                        g.fillPolygon(poly);
                        break;

                    case 6:
                        g.setColor(orange);
                        poly = new Polygon(x, y, 4);
                        g.drawPolygon(poly);
                        g.fillPolygon(poly);
                        break;
                }
            }
        }
    }
    public void showScore(){
        JFrame frame = new JFrame("Game over!");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(400, 300);
        JPanel panel = new JPanel();
        frame.add(panel);
        panel.add(new JLabel("Game over! You've got " + field.getScore() + " points! Well done!"));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
