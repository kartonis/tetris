package com.company.tetris;

import com.company.tetris.model.Field;

import java.awt.event.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class User implements KeyListener {
    private static int flag = 0;
    private static boolean mark = false;

    Lock l;
    Condition keyPressed;

    Field field;
    public User(Field f, Lock l, Condition keyPressed)
    {
        field = f;
        this.l = l;
        this.keyPressed = keyPressed;
    }
    public void keyPressed(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            if (field.isPaused()) return;
            field.rotateFigure();        }

        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            field.shiftFigureLeft();        }

        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            if (field.isPaused()) return;
            field.shiftFigureRight();
        }

        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            if (field.isPaused()) return;
            field.shiftFigureDown();
        }
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
            field.setPause();
        }
        l.lock();
        keyPressed.signal();
        l.unlock();
    }
    public void keyTyped(KeyEvent e){};
    public void keyReleased(KeyEvent e){};
}
